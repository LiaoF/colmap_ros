#ifndef _PATH_PLANNING_H_
#define _PATH_PLANNING_H_

#include <mutex>
#include <memory>
#include <ros/ros.h>

#include <octomap_msgs/conversions.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include "nus_msgs/PoseWithIdStamped.h"
#include "nus_msgs/StateStamped.h"
#include "Follower.h"
#include "Leader.h"

class PathPlanning
{
public:
    PathPlanning();
    ~PathPlanning();
    
private:
    void currentPoseCallback(const geometry_msgs::PoseStamped::ConstPtr& pose);
    void taskTargetPoseCallback(const nus_msgs::PoseWithIdStamped::ConstPtr& pose);
    void refStateCallback(const nus_msgs::StateStamped::ConstPtr& pose);
    void publishPath(const std::vector<Point<double> >& path);
    void publishLocalTargetPose(const geometry_msgs::Pose& pose);
    void mainLoop(const ros::TimerEvent&);
    void mapLoop(const ros::TimerEvent&);
    void leaderPosCallback(const geometry_msgs::PoseStamped::ConstPtr& p);
    bool resetOctomap();
private:
    std::string p_map_topic_;
    std::string p_current_pose_topic_;
    std::string p_task_target_pose_topic_;
    std::string p_reference_state_topic_;
    std::string p_mode_;
    int p_target_sysid_;
    
    /* map */
    Point<unsigned int> p_map_grid_max_;
    Point<unsigned int> p_map_grid_offset_;
    double p_map_grid_size_;
    double p_target_swap_threshold_;// threshold to check when the leader position should be used instead of tracking leader position
    /* ROS */
    ros::NodeHandle node_;
    ros::Subscriber map_sub_;
    ros::Subscriber current_pose_sub_;
    ros::Subscriber task_target_pose_sub_;
    ros::Subscriber ref_state_sub_;
    ros::Subscriber local_leader_pose_sub_;
    ros::Publisher  task_target_pose_pub_;
    ros::Publisher  local_target_pose_pub_;
    ros::Publisher  heading_target_pose_pub_; /* just for clear visualization */
    ros::Publisher  path_pub_;
    ros::Timer      timer_;
    ros::Timer      map_timer_;
    
    ros::ServiceClient octomap_reset_client_;
    ros::ServiceClient octomap_client_;
    
    /* Follower path planner */
    PathPlanner*    path_planner_;
    double ref_head_[3];
    double leaderPos_[3]; // in NEU coordinate 
    
    geometry_msgs::Pose current_pose_;
    nus_msgs::PoseWithIdStamped task_target_pose_;
    nus_msgs::StateStamped reference_state_;

    bool got_first_current_pose_;
    bool got_first_task_target_pose_;
    bool got_first_reference_state_;
    bool got_first_map_;
    bool got_map_;
    
    ros::Time octomap_last_reset_time_;
    
    /* multi-thread */
    std::mutex mutex_current_pose_;
    std::mutex mutex_map_octree_;
    std::mutex mutex_task_target_pose_;
    std::mutex mutex_accepted_path_;
    std::mutex mutex_reference_state_;
    std::mutex mutex_leader_pos_;
    
    bool* map_grid_;  // TODO: be careful!!!
    std::shared_ptr<visualization_msgs::Marker> map_octree_ptr_;
};

#endif /* _PATH_PLANNING_H_ */
