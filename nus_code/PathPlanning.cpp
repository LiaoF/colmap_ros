#include "PathPlanning.h"

#include <cmath>
#include <tf/transform_datatypes.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PointStamped.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/Path.h>
#include <std_srvs/Empty.h>
#include <octomap_msgs/GetOctomap.h>

PathPlanning::PathPlanning()
{
    /* ROS parameter */
    Point<int> map_grid_max;
    Point<int> map_grid_offset;
    Point<double> reflexxes_max_velocity;
    Point<double> reflexxes_max_acceleration;
    Point<double> reflexxes_max_jerk;
    
    ros::NodeHandle private_nh_("~");
    private_nh_.param("map_topic", p_map_topic_, std::string("/octomap_binary"));
    private_nh_.param("current_pose_topic", p_current_pose_topic_, std::string("/slam/pose"));
    private_nh_.param("task_target_pose_topic", p_task_target_pose_topic_, std::string("/tracking/target_pose"));
    private_nh_.param("reference_state_topic", p_reference_state_topic_, std::string("/reference/state"));
    private_nh_.param("map_grid_max_x", map_grid_max.x, 160);
    private_nh_.param("map_grid_max_y", map_grid_max.y, 160);
    private_nh_.param("map_grid_max_z", map_grid_max.z, 6);
    private_nh_.param("map_grid_offset_x", map_grid_offset.x, 80);
    private_nh_.param("map_grid_offset_y", map_grid_offset.y, 80);
    private_nh_.param("map_grid_offset_z", map_grid_offset.z, 0);
    private_nh_.param("map_grid_size", p_map_grid_size_, 0.5);
    private_nh_.param("target_swap_threshold", p_target_swap_threshold_, 4.0);
    private_nh_.param("target_sysid", p_target_sysid_, 1);
    private_nh_.param("mode", p_mode_, std::string("follower"));
    private_nh_.param("reflexxes_max_velocity_x", reflexxes_max_velocity.x, 0.5);
    private_nh_.param("reflexxes_max_velocity_y", reflexxes_max_velocity.y, 0.5);
    private_nh_.param("reflexxes_max_velocity_z", reflexxes_max_velocity.z, 1.0);
    private_nh_.param("reflexxes_max_acceleration_x", reflexxes_max_acceleration.x, 0.25);
    private_nh_.param("reflexxes_max_acceleration_y", reflexxes_max_acceleration.y, 0.25);
    private_nh_.param("reflexxes_max_acceleration_z", reflexxes_max_acceleration.z, 0.5);
    private_nh_.param("reflexxes_max_jerk_x", reflexxes_max_jerk.x, 0.8);
    private_nh_.param("reflexxes_max_jerk_y", reflexxes_max_jerk.y, 0.8);
    private_nh_.param("reflexxes_max_jerk_z", reflexxes_max_jerk.z, 0.5);
    
    p_map_grid_max_.x       = map_grid_max.x;
    p_map_grid_max_.y       = map_grid_max.y;
    p_map_grid_max_.z       = map_grid_max.z;
    p_map_grid_offset_.x    = map_grid_offset.x;
    p_map_grid_offset_.y    = map_grid_offset.y;
    p_map_grid_offset_.z    = map_grid_offset.z;
    
    /* ROS setup */
    current_pose_sub_       = node_.subscribe(p_current_pose_topic_, 1, &PathPlanning::currentPoseCallback, this);
    task_target_pose_sub_   = node_.subscribe(p_task_target_pose_topic_, 1, &PathPlanning::taskTargetPoseCallback, this);
    ref_state_sub_          = node_.subscribe(p_reference_state_topic_, 1, &PathPlanning::refStateCallback, this);
    path_pub_               = node_.advertise<nav_msgs::Path>("/pathplanning/path", 1);
    local_target_pose_pub_  = node_.advertise<geometry_msgs::PoseStamped>("/pathplanning/target_pose", 1);
    task_target_pose_pub_   = node_.advertise<nus_msgs::PoseWithIdStamped>("/pathplanning/target_pose_way_point", 1);
    heading_target_pose_pub_= node_.advertise<geometry_msgs::PoseStamped>("/pathplanning/heading_target_pose", 1); /* just for clear visualization */
    timer_                  = node_.createTimer(ros::Duration(1.0/50.0), &PathPlanning::mainLoop, this);
    map_timer_              = node_.createTimer(ros::Duration(1.0), &PathPlanning::mapLoop, this);
    octomap_reset_client_   = node_.serviceClient<std_srvs::Empty>("/colmap/reset");
    
    std::string sys_id = static_cast<std::ostringstream*>( &(std::ostringstream() << p_target_sysid_) )->str();
    local_leader_pose_sub_  = node_.subscribe("/mavros/position/local/sys_id_" + sys_id,1, &PathPlanning::leaderPosCallback, this);
   
    /* initialize follower path planner */
    unsigned int map_max[3];
    unsigned int map_offset[3];
    map_max[0]      = p_map_grid_max_.x;
    map_max[1]      = p_map_grid_max_.y;
    map_max[2]      = p_map_grid_max_.z;
    map_offset[0]   = p_map_grid_offset_.x;
    map_offset[1]   = p_map_grid_offset_.y;
    map_offset[2]   = p_map_grid_offset_.z;
    
    /* initialize path planner */
    if (0 == p_mode_.compare("follower"))
    {
        path_planner_ = new Follower();
    }
    else if (0 == p_mode_.compare("leader"))
    {
        path_planner_ = new Leader();
    }
    else
    {
        ROS_ERROR("mode error!");
        exit(EXIT_FAILURE);
    }
    path_planner_->init(map_max, map_offset, p_map_grid_size_, reflexxes_max_velocity, reflexxes_max_acceleration, reflexxes_max_jerk);
    
    /* initialize state */
    current_pose_.position.x = 0;
    current_pose_.position.y = 0;
    current_pose_.position.z = 0;
      
    got_first_current_pose_     = false;
    got_first_task_target_pose_ = false;
    got_first_reference_state_  = false;
    got_first_map_              = false;
    
    
    map_grid_ = new bool[p_map_grid_max_.x * p_map_grid_max_.y * p_map_grid_max_.z];
    
    octomap_last_reset_time_ = ros::Time::now();
}

PathPlanning::~PathPlanning()
{
    if (map_grid_)
    {
        delete map_grid_;
        map_grid_ = NULL;
    }
    if (path_planner_)
    {
        delete path_planner_;
        path_planner_ = NULL;
    }
}

void PathPlanning::mainLoop(const ros::TimerEvent&)
{
    /* wait for first data */
    if (!got_first_current_pose_ ||
        !got_first_task_target_pose_ ||
        !got_first_reference_state_ ||
        !got_first_map_)
    {
        return;
    }
    
    /* copy map pointer */
    mutex_map_octree_.lock();
    std::shared_ptr<octomap::OcTree> map_octree_ptr(map_octree_ptr_);
    mutex_map_octree_.unlock();
    
    /* get task target */
    nus_msgs::PoseWithIdStamped task_target_pose;
    mutex_task_target_pose_.lock();
    task_target_pose.pose.position      = task_target_pose_.pose.position;
    task_target_pose.pose.orientation   = task_target_pose_.pose.orientation;
    task_target_pose.id                 = task_target_pose_.id;
    mutex_task_target_pose_.unlock();

    /* get current pose */
    geometry_msgs::Pose current_pose;
    mutex_current_pose_.lock();
    current_pose.position       = current_pose_.position;
    current_pose.orientation    = current_pose_.orientation;
    mutex_current_pose_.unlock();
        
    /* get reference pose */
    nus_msgs::StateStamped reference_state;
    mutex_reference_state_.lock();
    reference_state.pose            = reference_state_.pose;
    reference_state.velocity        = reference_state_.velocity;
    reference_state.acceleration    = reference_state_.acceleration;
    mutex_reference_state_.unlock();
    
    /* get results from Follower path planner, NWU frame -> NEU frame */
    double real_position[3];
    real_position[0] =  current_pose.position.x;
    real_position[1] = -current_pose.position.y;
    real_position[2] =  current_pose.position.z;
    double newWP[3];
    newWP[0] =  task_target_pose.pose.position.x;
    newWP[1] = -task_target_pose.pose.position.y;
    newWP[2] =  task_target_pose.pose.position.z;
    
    // check the difference between the tracked leader and the communicated leader
    // if the difference is larger, use the communicated leader as the tracking target
  
    mutex_leader_pos_.lock();
    if (0 == p_mode_.compare("follower"))
    {
         double temp_dist;
        // check the xy square distance
        temp_dist = (newWP[0] - leaderPos_[0])*(newWP[0] - leaderPos_[0]) +  
                    (newWP[1] - leaderPos_[1])*(newWP[1] - leaderPos_[1]); //+(newWP[2] - leaderPos[2])*(newWP[2] - leaderPos[2]);
    
        if (temp_dist >= p_target_swap_threshold_)
        {
            ROS_INFO("Swapped: Tracking - leader = %2f", temp_dist);
            newWP[0] = leaderPos_[0];
            newWP[1] = leaderPos_[1];
            newWP[2] = leaderPos_[2];
        }
    }
    mutex_leader_pos_.unlock();
    
    /* update grid map from list of occupied nodes in visualization message from colmap */
    memset(map_grid_, 0, sizeof(bool) * p_map_grid_max_.x * p_map_grid_max_.y * p_map_grid_max_.z);
    for (octomap::OcTree::iterator it = map_octree_ptr->points.begin(), end = map_octree_ptr->points.end(); it != end; ++it)
    {
        // point position in grid map
        int x =  (it->x - current_pose.position.x) / p_map_grid_size_ + p_map_grid_offset_.x + 0.5;
        int y = -(it->y - current_pose.position.y) / p_map_grid_size_ + p_map_grid_offset_.y + 0.5;
        int z =  (it->z - current_pose.position.z) / p_map_grid_size_ + p_map_grid_offset_.z + 0.5;
        
        /* check data range */
        if (x < 0 || x >= p_map_grid_max_.x ||
            y < 0 || y >= p_map_grid_max_.y ||
            z < 0 || z >= p_map_grid_max_.z)
        {
            continue;
        }
        map_grid_[x + y * p_map_grid_max_.x + z * p_map_grid_max_.x * p_map_grid_max_.y] = true;
    }
    
    double ref_res[9];
    ref_res[0] =  reference_state.pose.position.x;
    ref_res[1] = -reference_state.pose.position.y;
    ref_res[2] =  reference_state.pose.position.z;
    ref_res[3] =  reference_state.velocity.linear.x;
    ref_res[4] = -reference_state.velocity.linear.y;
    ref_res[5] =  reference_state.velocity.linear.z;
    ref_res[6] =  reference_state.acceleration.linear.x;
    ref_res[7] = -reference_state.acceleration.linear.y;
    ref_res[8] =  reference_state.acceleration.linear.z;
    double target_pos[3];
    double output_path[50][3];
    double path_target[3];
    int    is_target_point_in_obstacle;
    int    is_start_point_in_obstacle;
    
    //ROS_INFO("\n\n new position = (%f, %f, %f)", real_position[0], real_position[1], real_position[2]);

    //ROS_INFO("new_wp = (%f, %f, %f)", newWP[0], newWP[1], newWP[2]);
    
    /* fixed hight to 1 meter*/
    //real_position[2] = 1.0;
    //newWP[2] = 1.0;
    
    path_planner_->main(map_grid_, real_position, newWP, ref_res, ref_head_, target_pos, output_path, path_target, is_target_point_in_obstacle, is_start_point_in_obstacle);
    //ROS_INFO("ref_res = (%f, %f, %f, %f, %f, %f, %f, %f, %f)", ref_res[0], ref_res[1], ref_res[2], ref_res[3], ref_res[4], ref_res[5], ref_res[6], ref_res[7], ref_res[8]);
    //ROS_INFO("target = (%f,  %f,  %f, %f)", target_pos[0], target_pos[1], target_pos[2], ref_head_[0]);
    //printf("========================================================\n");
    if (1 == is_start_point_in_obstacle)
    {
        ROS_INFO("start point in obstacle(NWU) = (%f, %f, %f)", real_position[0], -real_position[1], real_position[2]);
        
        /* NEU frame for map reading */
        int x = real_position[0] / p_map_grid_size_ + p_map_grid_offset_.x + 0.5;
        int y = real_position[1] / p_map_grid_size_ + p_map_grid_offset_.y + 0.5;
        int z = real_position[2] / p_map_grid_size_ + p_map_grid_offset_.z + 0.5;
        int circle = 3;
        if ((x - circle) >= 0 && (y - circle) >= 0 && z >= 0 && (x + circle) < p_map_grid_max_.x && (y + circle) < p_map_grid_max_.y && z < p_map_grid_max_.z)
        {
            for (int xx = x - circle; xx <= x + circle; xx++)
            {
                for (int yy = y - circle; yy <= y + circle; yy++)
                {
                    ROS_INFO("map[%d, %d, %d] = %d", xx, yy, z, map_grid_[xx + yy * p_map_grid_max_.x + z * p_map_grid_max_.x * p_map_grid_max_.y]);
                }
            }
        }
        else
        {
            ROS_WARN("position out of range: (%d, %d, %d)", x, y, z);
        }
    }

    /* get path and publish for visulization */
    if (path_pub_.getNumSubscribers() > 0)
    {
        std::vector<Point<double> > path;
        Point<double> point;
        for (int i = 0; i < ref_head_[2]; i++)
        {
            // FIXME: frame transform
            point.x =  (output_path[i][0] - p_map_grid_offset_.x) * p_map_grid_size_;
            point.y = -(output_path[i][1] - p_map_grid_offset_.y) * p_map_grid_size_;
            point.z =  (output_path[i][2] - p_map_grid_offset_.z) * p_map_grid_size_;
            path.push_back(point);
        }
        publishPath(path);
    }
    
    /* get local target and publish, NEU frame -> NWU frame */
    if (local_target_pose_pub_.getNumSubscribers() > 0)
    {
        geometry_msgs::Pose local_target;
        local_target.position.x =  target_pos[0];
        local_target.position.y = -target_pos[1];
        local_target.position.z =  target_pos[2];
        tf::Quaternion q;
        q.setRPY(0, 0, -ref_head_[0]); // FIXME
        tf::quaternionTFToMsg(q, local_target.orientation);
        publishLocalTargetPose(local_target);
    }
    
    /* tell task manager if task target is reachable */
    if (task_target_pose_pub_.getNumSubscribers() > 0 && 0 == p_mode_.compare("leader"))
    {
        if (1 == is_target_point_in_obstacle)
        {
            ROS_INFO("target unreachable, switch to new target");
            ROS_INFO("id = %d, target = (%f, %f, %f), path = (%f, %f, %f)", task_target_pose.id, newWP[0], -newWP[1], newWP[2], (output_path[0][0] - p_map_grid_offset_.x) * p_map_grid_size_, -(output_path[0][1] - p_map_grid_offset_.y) * p_map_grid_size_, (output_path[0][2] - p_map_grid_offset_.z) * p_map_grid_size_);
            nus_msgs::PoseWithIdStamped optimized_task_target;
            optimized_task_target.header.frame_id   = "/map";
            optimized_task_target.header.stamp      = ros::Time::now();
            optimized_task_target.pose.position.x   =  (output_path[0][0] - p_map_grid_offset_.x) * p_map_grid_size_;
            optimized_task_target.pose.position.y   = -(output_path[0][1] - p_map_grid_offset_.y) * p_map_grid_size_;
            optimized_task_target.pose.position.z   =  (output_path[0][2] - p_map_grid_offset_.z) * p_map_grid_size_;
            optimized_task_target.id                = task_target_pose.id;
            optimized_task_target.reachable_flag    = false;
            task_target_pose_pub_.publish(optimized_task_target);
        }
    }
    
    /* just for clear visualization */
    if (heading_target_pose_pub_.getNumSubscribers() > 0)
    {
        geometry_msgs::PoseStamped heading_target;
        heading_target.header.frame_id = "/map";
        heading_target.header.stamp = ros::Time::now();
        heading_target.pose.position =  current_pose.position;
        tf::Quaternion q;
        q.setRPY(0, 0, -ref_head_[0]); // FIXME
        tf::quaternionTFToMsg(q, heading_target.pose.orientation);
        heading_target_pose_pub_.publish(heading_target);
    }
}

void PathPlanning::mapLoop(const ros::TimerEvent&)
{
    if ((ros::Time::now() - octomap_last_reset_time_).toSec() < 4.0)
    {
        return;
    }
    
    // Look for a new visualization message from colmap with a timeout of 1 second
    boost::shared_ptr<visualization_msgs::Marker> msg = ros::topic::waitForMessage<visualization_msgs::Marker>("/colmap/occupied_cells_vis", node_, 1.0);
    if (!msg)
    {
        ROS_ERROR("Failed to retrieve map!");
        return;
    }
    
    mutex_map_octree_.lock();
    map_octree_ptr_ = msg;
    got_first_map_ = true;
    mutex_map_octree_.unlock();
    
    if (resetOctomap())
    {
        octomap_last_reset_time_ = ros::Time::now();
    }
}

void PathPlanning::currentPoseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
    mutex_current_pose_.lock();
    current_pose_.position      = msg->pose.position;
    current_pose_.orientation   = msg->pose.orientation;
    got_first_current_pose_     = true;
    mutex_current_pose_.unlock();
}

void PathPlanning::taskTargetPoseCallback(const nus_msgs::PoseWithIdStamped::ConstPtr& p)
{
    mutex_task_target_pose_.lock();
    task_target_pose_.pose.position     = p->pose.position;
    task_target_pose_.pose.orientation  = p->pose.orientation;
    task_target_pose_.id                = p->id;
    got_first_task_target_pose_         = true;
    mutex_task_target_pose_.unlock();
}

void PathPlanning::leaderPosCallback(const geometry_msgs::PoseStamped::ConstPtr& p)
{
    //ROS_INFO("leaderPosCallback ");
     mutex_leader_pos_.lock();
    // from ENU to NEU
    leaderPos_[0] = p->pose.position.y;
    leaderPos_[1] = p->pose.position.x;
    leaderPos_[2] = p->pose.position.z;
    mutex_leader_pos_.unlock();
}
void PathPlanning::publishPath(const std::vector<Point<double> >& path)
{
    nav_msgs::Path msg;
    msg.header.stamp = ros::Time::now();
    msg.header.frame_id = "/map";
    int size = path.size();
    msg.poses.resize(size);
    for (int i = 0; i < size; i++)
    {
        msg.poses[i].pose.position.x = path[i].x;
        msg.poses[i].pose.position.y = path[i].y;
        msg.poses[i].pose.position.z = path[i].z;
        msg.poses[i].pose.orientation.x = 0;
        msg.poses[i].pose.orientation.y = 0;
        msg.poses[i].pose.orientation.z = 0;
        msg.poses[i].pose.orientation.w = 1;
    }
    path_pub_.publish(msg);
}

void PathPlanning::publishLocalTargetPose(const geometry_msgs::Pose& pose)
{
    geometry_msgs::PoseStamped msg;
    msg.header.stamp = ros::Time::now();
    msg.header.frame_id = "/map";
    msg.pose = pose;
    local_target_pose_pub_.publish(msg);
}

void PathPlanning::refStateCallback(const nus_msgs::StateStamped::ConstPtr& p)
{
    mutex_reference_state_.lock();
    reference_state_ = *p;
    got_first_reference_state_ = true;
    mutex_reference_state_.unlock();
}

bool PathPlanning::resetOctomap()
{
    bool rc = false;
    std_srvs::Empty octomap_srv;
    if (octomap_reset_client_.call(octomap_srv))
    {
        //ROS_INFO("reset octomap!");
        rc = true;
    }
    else
    {
        ROS_ERROR("Failed to call service octomap!");
        rc = false;
    }
    return rc;
}
