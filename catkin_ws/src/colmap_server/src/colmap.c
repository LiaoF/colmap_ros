// ----------------------------------------------------------------------------------
//
//  Filename:     colmap.c
//  Author:       Alex Fisher <alex.fisher@rmit.edu.au>
//  Description:  Probablistic mapping
//
// ----------------------------------------------------------------------------------

// Includes
// ----------------------------------------------------------------------------------
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include "colmap.h"

// Defines
// -----------------------------------------------------------------------------------

// These flags are stored in the "type" word of a leaf,
// along with the starting z location (for a compressed leaf)
#define HDR_COMPRESSED    0x0001
#define HDR_START_SHIFT   1

// In a compressed leaf, this flag indicates whether an element is a run or a single prob
#define ELEM_RUN      0x0001

// In an uncompressed leaf, these flags indicate whether a cell has been hit or missed in
// the current scan and subsequently needs updating
#define PROB_DIRTY_MASK 0x0003
#define PROB_HIT_FLAG   0x0001
#define PROB_MISS_FLAG  0x0002

// These flags indicate the type of a RUN element
#define ELEM_TERM     0x0002
#define ELEM_TYPE_MASK 0x000C
#define ELEM_NORMAL   0x0000
#define ELEM_PROB_MIN 0x0004
#define ELEM_PROB_MAX 0x0008
#define ELEM_UNKNOWN  0x000C

#define ELEM_RUNLEN_MASK 0xFFF0
#define ELEM_RUNLEN_SHIFT 4

// Probability limits and increments ( -2 < logodds < 3.5 )
// Increment 0.85 for hit and -0.4 for miss (in Octomap paper its 0.85 and -0.4)
// Using fixed-point s3.12
// (however last 2 bits used for something else, must ALWAYS BE ZERO for deltas and min/max)
// Really need to watch for wraparound here!!!
#define PROB_MIN -8192
#define PROB_MAX  14336
#define DELTA_PROB_HIT 3480
#define DELTA_PROB_MISS -1636
// TEMP: For faulty limiting in Octomap velodyne data
//#define PROB_MIN -16322
//#define PROB_MAX  28476

// Macros
// -----------------------------------------------------------------------------------
// Little helper
#define FOR3 for (uint32_t _=0;_<3;_++)
#define MEM(x) mem_usage += (x);

// DEPTH is the number of levels of inner nodes (including the root)
// or in other words the total number of levels (excluding the root)

// Typedefs
// -----------------------------------------------------------------------------------
  
// Prototypes
// -----------------------------------------------------------------------------------
static float squared_distance(float v1[3], float v2[3]);
static inline prob_t clamp_prob(prob_t prob);
static inline prob_t incorporate_hit_miss(prob_t prob);
static leaf_t ** get_leaf_with_alloc(tree_node_t *root, int32_t x, int32_t y, uint32_t depth);
static leaf_t * get_leaf(tree_node_t *root, int32_t x, int32_t y, uint32_t depth);
static uint32_t compress_leaf(leaf_t *src, leaf_t *dest, uint32_t nz);
static void compress_leaf_in_tree(leaf_t **leaf, uint32_t nz);
static void compress_all_leafs(tree_node_t **node, uint32_t depth, uint32_t nz);
static uint32_t decompress_leaf(leaf_t *src, leaf_t *dest, uint32_t nz);
static void decompress_leaf_in_tree(leaf_t **leaf, uint32_t nz);
static void decompress_all_leafs(tree_node_t **node, uint32_t depth, uint32_t nz);
static inline void update_cell_direct(tree_node_t *root, int32_t pt[3], prob_t delta_prob, uint32_t nz, uint32_t depth);
static inline void update_cell_hit_miss(tree_node_t *root, int32_t pt[3], prob_t hit_or_miss, uint32_t nz, uint32_t depth);
static void iterate_occupied_cells(tree_node_t *node, int32_t depth, int32_t x, int32_t y, void (*cb)(int32_t x, int32_t y, int32_t z));

// Globals
// -----------------------------------------------------------------------------------
static uint32_t mem_usage = 0;
static stats_t stats;

// Math functions
// -----------------------------------------------------------------------------------
static float squared_distance(float v1[3], float v2[3])
{
  return (v2[0]-v1[0])*(v2[0]-v1[0]) + (v2[1]-v1[1])*(v2[1]-v1[1]) + (v2[2]-v1[2])*(v2[2]-v1[2]);
}

static inline prob_t clamp_prob(prob_t prob)
{
  if (prob > PROB_MAX) { return PROB_MAX; }
  else if (prob < PROB_MIN) { return PROB_MIN; }
  return prob;
}

static inline prob_t incorporate_hit_miss(prob_t prob)
{
  // Hits take precedence over misses
  if (prob & PROB_DIRTY_MASK) {
    if (prob & PROB_HIT_FLAG) {
      return clamp_prob((prob & ~PROB_DIRTY_MASK) + DELTA_PROB_HIT);
    } else {
      return clamp_prob((prob & ~PROB_DIRTY_MASK) + DELTA_PROB_MISS);
    }
  } else {
    return prob;
  }
}

// Generic N-ary tree functions
// -----------------------------------------------------------------------------------
static leaf_t ** get_leaf_with_alloc(tree_node_t *root, int32_t x, int32_t y, uint32_t depth)
{
  // Returns POINTER to tree's leaf pointer to allow for easy updating of the tree
  // x,y are map indices (can be pos or neg)
  // Allocates interior nodes, but does not allocate leaf
  // TODO: Could easily limit depth to less than max to perform partial query
  tree_node_t *node = root;
  uint32_t xi, yi;
  int32_t i;

  // Expand all inner nodes
  for (i=depth-1; i>0; i--) {
    xi = (x >> (i*LOG_N)) & (N-1);
    yi = (y >> (i*LOG_N)) & (N-1);

    if (!node->child[xi][yi]) {
      node->child[xi][yi] = calloc(1, sizeof(tree_node_t));
      MEM(sizeof(tree_node_t));
    }
    node = node->child[xi][yi];
  }

  // At the last inner node just return ptr to child pointer
  xi = (x >> (i*LOG_N)) & (N-1);
  yi = (y >> (i*LOG_N)) & (N-1);

  return (leaf_t**)&node->child[xi][yi];
}

static leaf_t * get_leaf(tree_node_t *root, int32_t x, int32_t y, uint32_t depth)
{
  // Returns leaf pointer
  // x,y are map indices (can be pos or neg)
  // Does not allocate any nodes (returns NULL if non-existant)
  // TODO: Could easily limit depth to less than max to perform partial query
  tree_node_t *node = root;
  uint32_t xi, yi;
  int32_t i;

  // Expand all inner nodes
  for (i=depth-1; i>0; i--) {
    xi = (x >> (i*LOG_N)) & (N-1);
    yi = (y >> (i*LOG_N)) & (N-1);

    if (!node->child[xi][yi]) { return NULL; }
    node = node->child[xi][yi];
  }

  // At the last inner node just return ptr to child pointer
  xi = (x >> (i*LOG_N)) & (N-1);
  yi = (y >> (i*LOG_N)) & (N-1);

  return (leaf_t*)node->child[xi][yi];
}

static void iterate_leafs(tree_node_t *node, int32_t depth, int32_t x, int32_t y, void (*cb)(leaf_t *leaf, int32_t x, int32_t y))
{
  // If depth zero on initial call, x and y will be invalid
  if (!depth) {
    cb((leaf_t*)node, x, y);
  } else {
    for (int32_t i=0; i<N; i++) {
      for (int32_t j=0; j<N; j++) {
        if (node->child[i][j]) {
          iterate_leafs(node->child[i][j], depth-1, (x << LOG_N) | i, (y << LOG_N) | j, cb);
        }
      }
    }
  }
}

static void iterate_occupied_cells(tree_node_t *node, int32_t depth, int32_t x, int32_t y, void (*cb)(int32_t x, int32_t y, int32_t z))
{
  // If depth zero on initial call, x and y will be invalid
  if (!depth) {

    // Execute the callback for every occupied cell
    leaf_t *src = (leaf_t*)node;
    
    // Assume compressed
    assert(src->hdr & HDR_COMPRESSED);

    // Assume at least 1 valid element!!
    uint32_t z = (src->hdr >> HDR_START_SHIFT);
    uint32_t i = 0;
    elem_t elem;

    while (1) {
      //assert(i < MAX_ELEMS);
      elem = src->c.elem[i++];

      if (elem & ELEM_RUN) {
        // It's a run
        switch (elem & ELEM_TYPE_MASK) {
          case ELEM_UNKNOWN:
            z += (elem >> ELEM_RUNLEN_SHIFT);
            break;
          case ELEM_PROB_MIN:
            z += (elem >> ELEM_RUNLEN_SHIFT);
            break;
          case ELEM_PROB_MAX:
            for (uint32_t j=0;j<(elem >> ELEM_RUNLEN_SHIFT);j++) { cb(x, y, z++); }
            break;
          case ELEM_NORMAL:
            if (((prob_t)(src->c.elem[i] & ~ELEM_RUN)) > 0) {
              for (uint32_t j=0;j<(elem >> ELEM_RUNLEN_SHIFT);j++) { cb(x, y, z++); }
            } else {
              z += (elem >> ELEM_RUNLEN_SHIFT);
            }
            i++;
            break;
        }

        if (elem & ELEM_TERM) { break; }

      } else {
        // It's a normal prob
        if (((prob_t)elem) > 0) { cb(x, y, z++); }
      }
    }

  } else {
    for (int32_t i=0; i<N; i++) {
      for (int32_t j=0; j<N; j++) {
        if (node->child[i][j]) {
          iterate_occupied_cells(node->child[i][j], depth-1, (x << LOG_N) | i, (y << LOG_N) | j, cb);
        }
      }
    }
  }
}

static void delete_node(tree_node_t *node, uint32_t depth)
{
  if (depth) {
    for (int32_t i=0; i<N; i++) {
      for (int32_t j=0; j<N; j++) {
        if (node->child[i][j]) {
          delete_node(node->child[i][j], depth-1);
        }
      }
    }
  }

  free(node);
}

// Leaf compression
// -----------------------------------------------------------------------------------
static uint32_t compress_leaf(leaf_t *src, leaf_t *dest, uint32_t nz)
{
  assert(!(src->hdr & HDR_COMPRESSED));

  uint32_t i;
  uint32_t run_len;

  // Find first non-zero cell
  // Assume starting point of an uncompressed leaf (and therefore the whole map) is zero
  // Note this will also find a cell with zero prob but a hit/miss flag set
  // IMPORTANT: Need to catch the cases where the hit/miss flag will bring the new prob to zero :-/
  //            Could just call incorporate_hit_miss but this should be faster
  for (i=0;i<nz;i++) {
    if (src->u.prob[i] && 
        (src->u.prob[i] != (-DELTA_PROB_MISS | PROB_MISS_FLAG)) &&
        (src->u.prob[i] != (-DELTA_PROB_HIT | PROB_MISS_FLAG | PROB_HIT_FLAG)) &&
        (src->u.prob[i] != (-DELTA_PROB_HIT | PROB_HIT_FLAG)))
    { break; }
  }

  // In the case of all zeros, return 0
  if (i == nz) { return 0; }

  // Fill out header
  dest->hdr = HDR_COMPRESSED | (i << HDR_START_SHIFT);

  // Increment i and save previous values
  // Incorporate hit/miss for the first value if necessary
  prob_t prev_p = incorporate_hit_miss(src->u.prob[i]);
  uint32_t n_elem = 0;
  uint32_t prev_i = i;

  for (i++; i<nz; i++) {
    // Obtain next prob and incorporate hit/miss
    prob_t cur_p = incorporate_hit_miss(src->u.prob[i]);

    if (cur_p != prev_p) {
      run_len = i - prev_i;

      if (run_len == 1) {
        // Regular element, store probability with cleared run flag
        dest->c.elem[n_elem++] = prev_p;
      } else {
        // It's a run
        if (!prev_p) {
          dest->c.elem[n_elem++] = ELEM_UNKNOWN | ELEM_RUN | (run_len << ELEM_RUNLEN_SHIFT);
        } else if (prev_p == PROB_MIN) {
          dest->c.elem[n_elem++] = ELEM_PROB_MIN | ELEM_RUN | (run_len << ELEM_RUNLEN_SHIFT);
        } else if (prev_p == PROB_MAX) {
          dest->c.elem[n_elem++] = ELEM_PROB_MAX | ELEM_RUN | (run_len << ELEM_RUNLEN_SHIFT);
        } else {
          dest->c.elem[n_elem++] = ELEM_NORMAL | ELEM_RUN | (run_len << ELEM_RUNLEN_SHIFT);
          dest->c.elem[n_elem++] = prev_p;
        }
      }
      prev_p = cur_p;
      prev_i = i;
    }
  }


  // Last element must be a run unfortunately (may waste some space)
  run_len = nz - prev_i;

  if (!prev_p) {
    // Finished on an unknown run; don't insert it, just make previous one a terminal run
    if (dest->c.elem[n_elem-1] & ELEM_RUN) {
      // Last was a min/max/unknown run, make it term
      dest->c.elem[n_elem-1] |= ELEM_TERM;
    } else {
      if ((n_elem > 1) && (dest->c.elem[n_elem-2] & ELEM_RUN)) {
        if (dest->c.elem[n_elem-2] & ELEM_TYPE_MASK) {
          // 2nd to last was a min/max/unknown run, therefore last was a single
          // Make it a length 1 run
          if (dest->c.elem[n_elem-1] == (elem_t)PROB_MAX) {
            dest->c.elem[n_elem-1] = ELEM_PROB_MAX | ELEM_RUN | ELEM_TERM | (1 << ELEM_RUNLEN_SHIFT);
          } else if (dest->c.elem[n_elem-1] == (elem_t)PROB_MIN) {
            dest->c.elem[n_elem-1] = ELEM_PROB_MIN | ELEM_RUN | ELEM_TERM | (1 << ELEM_RUNLEN_SHIFT);
          } else {
            dest->c.elem[n_elem] = dest->c.elem[n_elem-1];
            dest->c.elem[n_elem-1] = ELEM_NORMAL | ELEM_RUN | ELEM_TERM | (1 << ELEM_RUNLEN_SHIFT);
            n_elem++;
          }
        } else {
          // Last was a normal run, make it term
          dest->c.elem[n_elem-2] |= ELEM_TERM;
        }
      } else {
        // Last was a single
        if (dest->c.elem[n_elem-1] == (elem_t)PROB_MAX) {
          dest->c.elem[n_elem-1] = ELEM_PROB_MAX | ELEM_RUN | ELEM_TERM | (1 << ELEM_RUNLEN_SHIFT);
        } else if (dest->c.elem[n_elem-1] == (elem_t)PROB_MIN) {
          dest->c.elem[n_elem-1] = ELEM_PROB_MIN | ELEM_RUN | ELEM_TERM | (1 << ELEM_RUNLEN_SHIFT);
        } else {
          dest->c.elem[n_elem] = dest->c.elem[n_elem-1];
          dest->c.elem[n_elem-1] = ELEM_NORMAL | ELEM_RUN | ELEM_TERM | (1 << ELEM_RUNLEN_SHIFT);
          n_elem++;
        }
      }
    }
  } else if (prev_p == PROB_MIN) {
    dest->c.elem[n_elem++] = ELEM_PROB_MIN | ELEM_RUN | ELEM_TERM | (run_len << ELEM_RUNLEN_SHIFT);
  } else if (prev_p == PROB_MAX) {
    dest->c.elem[n_elem++] = ELEM_PROB_MAX | ELEM_RUN | ELEM_TERM | (run_len << ELEM_RUNLEN_SHIFT);
  } else {
    dest->c.elem[n_elem++] = ELEM_NORMAL | ELEM_RUN | ELEM_TERM | (run_len << ELEM_RUNLEN_SHIFT);
    dest->c.elem[n_elem++] = prev_p;
  }

  return sizeof(compressed_leaf_t) + n_elem*sizeof(elem_t);
}

static void compress_leaf_in_tree(leaf_t **leaf, uint32_t nz)
{
  // Malloc new, compress, free old data, update pointer in tree
  leaf_t *dest = malloc(sizeof(compressed_leaf_t) + (nz+8)*sizeof(elem_t));
  uint32_t size = compress_leaf(*leaf, dest, nz);
  free(*leaf);
  if (size) {
    *leaf = realloc(dest, size);
  } else {
    *leaf = NULL;
    free(dest);
  }
  MEM(size - sizeof(uncompressed_leaf_t));
}

static void compress_all_leafs(tree_node_t **node, uint32_t depth, uint32_t nz)
{
  if (!depth) {
    if (!(((leaf_t*)(*node))->hdr & HDR_COMPRESSED)) {
      compress_leaf_in_tree((leaf_t**)node, nz);
    }
  } else {
    for (int32_t i=0; i<N; i++) {
      for (int32_t j=0; j<N; j++) {
        if ((*node)->child[i][j]) {
          compress_all_leafs(&((*node)->child[i][j]), depth-1, nz);
        }
      }
    }
  }
}

// Leaf decompression
// -----------------------------------------------------------------------------------
static uint32_t decompress_leaf(leaf_t *src, leaf_t *dest, uint32_t nz)
{
  assert(src->hdr & HDR_COMPRESSED);

  dest->hdr = 0;

  // Assume at least 1 valid element!!
  uint32_t z;
  uint32_t i = 0;
  elem_t elem;

  // Cells before starting point are unknown
  // Assume starting point of an uncompressed leaf (and therefore the whole map) is zero
  for (z=0;z<(src->hdr >> HDR_START_SHIFT);z++) {
    dest->u.prob[z] = 0;
  }

  while (1) {
    //assert(i < MAX_ELEMS);
    elem = src->c.elem[i++];

    if (elem & ELEM_RUN) {
      // It's a run
      switch (elem & ELEM_TYPE_MASK) {
        case ELEM_UNKNOWN:
          for (uint32_t j=0;j<(elem >> ELEM_RUNLEN_SHIFT);j++) { dest->u.prob[z++] = 0; }
          break;
        case ELEM_PROB_MIN:
          for (uint32_t j=0;j<(elem >> ELEM_RUNLEN_SHIFT);j++) { dest->u.prob[z++] = PROB_MIN; }
          break;
        case ELEM_PROB_MAX:
          for (uint32_t j=0;j<(elem >> ELEM_RUNLEN_SHIFT);j++) { dest->u.prob[z++] = PROB_MAX; }
          break;
        case ELEM_NORMAL:
          for (uint32_t j=0;j<(elem >> ELEM_RUNLEN_SHIFT);j++) { dest->u.prob[z++] = src->c.elem[i] & ~ELEM_RUN; }
          i++;
          break;
      }

      if (elem & ELEM_TERM) { break; }

    } else {
      // It's a normal prob
      dest->u.prob[z++] = elem;
    }
  }

  for ( ; z<nz; z++) { dest->u.prob[z] = 0; }

  // Return size of original compressed structure
  return sizeof(compressed_leaf_t) + i*sizeof(elem_t);
}

static void decompress_leaf_in_tree(leaf_t **leaf, uint32_t nz)
{
  // Malloc new, decompress, free old data, update pointer in tree
  leaf_t *dest = malloc(sizeof(uncompressed_leaf_t) + nz*sizeof(prob_t));
  assert(dest);
  uint32_t size = decompress_leaf(*leaf, dest, nz);
  free(*leaf);
  *leaf = dest;
  MEM(sizeof(uncompressed_leaf_t) - size);
}

static void decompress_all_leafs(tree_node_t **node, uint32_t depth, uint32_t nz)
{
  if (!depth) {
    if (((leaf_t*)(*node))->hdr & HDR_COMPRESSED) {
      decompress_leaf_in_tree((leaf_t**)node, nz);
    }
  } else {
    for (int32_t i=0; i<N; i++) {
      for (int32_t j=0; j<N; j++) {
        if ((*node)->child[i][j]) {
          decompress_all_leafs(&((*node)->child[i][j]), depth-1, nz);
        }
      }
    }
  }
}

/* void decompress_all_within_radius(tree_node_t **node, int32_t depth, int32_t xc, int32_t yc, int32_t radius)
{
  static int32_t ix = 0, iy = 0;

  if (!depth) {
    // Sign extend -- DEPENDENT ON DEPTH!! Only valid for DEPTH = 4!!
    assert(DEPTH == 4);
    int32_t x = (ix & 0x00008000) ? (ix | 0xFFFF0000) : ix;
    int32_t y = (iy & 0x00008000) ? (iy | 0xFFFF0000) : iy;

    float dist = sqrtf((x-xc)*(x-xc) + (y-yc)*(y-yc));

    if ((dist < radius) && (((leaf_t*)(*node))->hdr & HDR_COMPRESSED)) {
      decompress_leaf_in_tree((leaf_t**)node);
    }

  } else {
    for (int32_t i=0; i<N; i++) {
      for (int32_t j=0; j<N; j++) {
        if ((*node)->child[i][j]) {
          uint32_t shift = (depth-1)*LOG_N;
          ix = (ix & ~((N-1) << shift)) | (i << shift);
          iy = (iy & ~((N-1) << shift)) | (j << shift);
          decompress_all_within_radius(&(*node)->child[i][j], depth-1, xc, yc, radius);
        }
      }
    }
  }
} */

// Map update/query functions
// -----------------------------------------------------------------------------------
/* void to_max_likelihood(tree_node_t **node, int32_t depth, bool compress)
{
  if (!depth) {
    if (((leaf_t*)(*node))->hdr & HDR_COMPRESSED) {
      decompress_leaf_in_tree((leaf_t**)node);
      for (int32_t i=0; i<NZ; i++) {
        if ((*((leaf_t**)node))->u.prob[i] > 0) {
          (*((leaf_t**)node))->u.prob[i] = PROB_MAX;
        } else if ((*((leaf_t**)node))->u.prob[i] < 0) {
          (*((leaf_t**)node))->u.prob[i] = PROB_MIN;
        }
      }
      if (compress) { compress_leaf_in_tree((leaf_t**)node); }
    }
  } else {
    for (int32_t i=0; i<N; i++) {
      for (int32_t j=0; j<N; j++) {
        if ((*node)->child[i][j]) {
          to_max_likelihood(&((*node)->child[i][j]), depth-1, compress);
        }
      }
    }
  }
}
*/ 

/* void update_cell(tree_node_t *root, int32_t pt[3], int32_t delta_prob, bool decompress)
{

  leaf_t **leaf = (leaf_t**)get_leaf_with_alloc(root, pt[0], pt[1]);

  // What type of leaf do we have?
  if (!(*leaf)) {
    // It's a brand new leaf (compressed), fill it with a single terminal element
    MEM(sizeof(compressed_leaf_min_t));
    *leaf = malloc(sizeof(compressed_leaf_min_t));
    (*leaf)->hdr = HDR_COMPRESSED | (pt[2] << HDR_START_SHIFT);
    ((compressed_leaf_t*)(*leaf))->elem[0] = 0x0010 | ELEM_RUN | ELEM_TERM;
    ((compressed_leaf_t*)(*leaf))->elem[1] = delta_prob;

  } else if ((*leaf)->hdr & HDR_COMPRESSED) { 
    if (decompress) {
      // It's a compressed leaf that should be decompressed and updated (OK)
      decompress_leaf_in_tree((compressed_leaf_t**)leaf);
      uncompressed_leaf_t *uc_leaf = (uncompressed_leaf_t*)(*leaf);
      uc_leaf->prob[pt[2]] = clamp_prob(uc_leaf->prob[pt[2]] + delta_prob);

    } else {
      // It's a compressed leaf that should be updated while compressed (bad)
      // For now just naively decompress->update->recompress
      uncompressed_leaf_t temp_leaf;
      uint32_t old_size = decompress_leaf((compressed_leaf_t*)(*leaf), &temp_leaf);
      free(*leaf);
      temp_leaf.prob[pt[2]] += delta_prob;
      compressed_leaf_t *new_leaf = malloc(sizeof(compressed_leaf_max_t));
      uint32_t new_size = compress_leaf(&temp_leaf, new_leaf);
      if (new_size) {
        *leaf = realloc(new_leaf, new_size);
      } else {
        *leaf = NULL;
        free(new_leaf);
      }
      MEM(new_size - old_size);
    }
  } else {
    // It's an uncompressed leaf that should be updated (good)
    uncompressed_leaf_t *uc_leaf = (uncompressed_leaf_t*)(*leaf);
    uc_leaf->prob[pt[2]] = clamp_prob(uc_leaf->prob[pt[2]] + delta_prob);
  }
}
*/

static inline void update_cell_direct(tree_node_t *root, int32_t pt[3], prob_t delta_prob, uint32_t nz, uint32_t depth)
{
  // Decompresses the leaf if necessary, then directly adds delta_prob to the
  // existing probability value

  // Get the leaf (create if necessary)
  leaf_t **leaf = (leaf_t**)get_leaf_with_alloc(root, pt[0], pt[1], depth);

  // What type of leaf do we have?
  if (!(*leaf)) {
    // It's a brand new leaf, allocate it uncompressed
    MEM(sizeof(uncompressed_leaf_t));
    *leaf = calloc(1, sizeof(uncompressed_leaf_t) + nz*sizeof(prob_t));
  } else if ((*leaf)->hdr & HDR_COMPRESSED) { 
    // It's a compressed leaf, decompress it
    decompress_leaf_in_tree(leaf, nz);
  }
  (*leaf)->u.prob[pt[2]] = clamp_prob((*leaf)->u.prob[pt[2]] + delta_prob);
}

static inline void update_cell_hit_miss(tree_node_t *root, int32_t pt[3], prob_t hit_or_miss, uint32_t nz, uint32_t depth)
{
  // Decompresses the leaf if necessary, then marks the cell as
  // hit or miss. Note, incorporate_hits_and_misses must be run afterwards to
  // update the probabilities, once all cells in a scan have been marked.
  // This can be skipped if compress_all_leafs is being called immediately, as this will
  // be done as part of the compression.

  // Get the leaf (create if necessary)
  leaf_t **leaf = (leaf_t**)get_leaf_with_alloc(root, pt[0], pt[1], depth);

  // What type of leaf do we have?
  if (!(*leaf)) {
    // It's a brand new leaf, allocate it uncompressed
    MEM(sizeof(uncompressed_leaf_t));
    *leaf = calloc(1, sizeof(uncompressed_leaf_t) + nz*sizeof(prob_t));
  } else if ((*leaf)->hdr & HDR_COMPRESSED) { 
    // It's a compressed leaf, decompress it
    decompress_leaf_in_tree(leaf, nz);
  }
  (*leaf)->u.prob[pt[2]] |= hit_or_miss;
}

// Public functions
// -----------------------------------------------------------------------------------
colmap_t * colmap_create(float cell_size, float min_z, uint32_t nz, float max_scan_dist)
{
  colmap_t *map = malloc(sizeof(colmap_t));

  map->root = calloc(1, sizeof(tree_node_t));
  map->inv_cell_size = 1.0 / cell_size;
  map->min_z = min_z;
  map->max_scan_dist_sq = max_scan_dist*max_scan_dist;

  map->depth = 4;
  map->nz = nz;

  MEM(sizeof(tree_node_t) + sizeof(colmap_t));

  return map;
}

void colmap_delete(colmap_t *map)
{
  delete_node(map->root, map->depth);
  free(map);
}

void colmap_compress(colmap_t *map)
{
  compress_all_leafs(&map->root, map->depth, map->nz);
}

void colmap_decompress(colmap_t *map)
{
  decompress_all_leafs(&map->root, map->depth, map->nz);
}

void colmap_insert_ray(colmap_t *map, float origin[3], float point[3])
{
  // Assume start is in bounds, only check end for out-of-bounds

  // Convert to map coordinates
  float ray_start[3], ray_end[3];

  ray_start[0] = origin[0] * map->inv_cell_size;
  ray_start[1] = origin[1] * map->inv_cell_size;
  ray_start[2] = (origin[2] - map->min_z) * map->inv_cell_size;

  ray_end[0] = point[0] * map->inv_cell_size;
  ray_end[1] = point[1] * map->inv_cell_size;
  ray_end[2] = (point[2] - map->min_z) * map->inv_cell_size;

  // Figure vector between
  float dx[3];
  FOR3 { dx[_] = ray_end[_] - ray_start[_]; }


  // Which will be the independent coordinate in the ray trace?
  uint32_t ind_coord;
  if ((fabs(dx[0]) >= fabs(dx[1])) && (fabs(dx[0]) >= fabs(dx[2]))) {
    ind_coord = 0;
  } else if ((fabs(dx[1]) >= fabs(dx[0])) && (fabs(dx[1]) >= fabs(dx[2]))) {
    ind_coord = 1;
  } else {
    ind_coord = 2;
  }

  // Which way are we stepping the independent coord?
  int32_t di = (ray_end[ind_coord] > ray_start[ind_coord]) ? 1 : -1;

  // How much are we stepping all three coordinates each time?
  float step_x[3];
  FOR3 { step_x[_] = dx[_] / fabs(dx[ind_coord]); }

  // Perform the ray trace
  bool oob = false;
  int32_t ii = 0;
  int32_t pt[3];
  int32_t last_i = lrint(ray_end[ind_coord]); 

  for (int32_t i = lrint(ray_start[ind_coord]); i != last_i; i += di) {

    FOR3 { pt[_] = lrint(ray_start[_] + ii*step_x[_]); }

    // TODO check inbounds horizontally
    // All uncompressed leafs start at zero for now
    if ((pt[2] >= 0) && (pt[2] < map->nz)) {
      update_cell_hit_miss(map->root, pt, PROB_MISS_FLAG, map->nz, map->depth);
    } else {
      oob = true;
      break;
    }
    
    ii++;
  }

  // Mark last cell in ray as a hit if we didn't already go out of bounds
  if (!oob) {
    FOR3 { pt[_] = lrint(ray_start[_] + ii*step_x[_]); }
    if ((pt[2] >= 0) && (pt[2] < map->nz)) {
      update_cell_hit_miss(map->root, pt, PROB_HIT_FLAG, map->nz, map->depth);
    }
  }
}

void colmap_insert_pcl(colmap_t *map, float origin[3], float points[][3], uint32_t n_pts)
{
  // Insert all rays in the point cloud using a common origin
  // Reject any that are too long
  for (uint32_t i=0; i<n_pts; i++) {
    if (squared_distance(origin, points[i]) <= map->max_scan_dist_sq) { 
      colmap_insert_ray(map, origin, points[i]);
    }
  }
}

prob_t colmap_query_cell(colmap_t *map, float pt[3])
{
  leaf_t *leaf = get_leaf(map->root, lrint(pt[0] * map->inv_cell_size), lrint(pt[1] * map->inv_cell_size), map->depth);
  int32_t zq = lrint(pt[2] * map->inv_cell_size);

  // Leaf doesn't exist?
  if (!leaf) { return 0; }

  if (leaf->hdr & HDR_COMPRESSED) {
    // We have a compressed leaf, that's annoying
    int32_t z = leaf->hdr >> HDR_START_SHIFT;

    // Is it below the starting point of the leaf?
    if (zq < z) { return 0; }

    elem_t elem;
    int32_t i = 0;
    uint32_t elem_type;
    while (1) {
      //assert(i < MAX_ELEMS);
      elem = leaf->c.elem[i++];

      if (elem & ELEM_RUN) {
        // It's a run
        z += elem >> ELEM_RUNLEN_SHIFT;
        elem_type = elem & ELEM_TYPE_MASK;
        if (zq < z) {
          // Found it!
          switch (elem_type) {
            case ELEM_UNKNOWN:    return 0;
            case ELEM_PROB_MIN:   return PROB_MIN;
            case ELEM_PROB_MAX:   return PROB_MAX;
            case ELEM_NORMAL:     return (prob_t)(leaf->c.elem[i] & ~ELEM_RUN);
          }
        }

        // Don't forget to skip over the probability value for a normal run element
        if (elem_type == ELEM_NORMAL) { i++; }

        // Couldn't find it!
        if (elem & ELEM_TERM) { return 0; }

      } else {
        // It's a normal prob
        z++;
        if (zq < z) {
          // Found it!
          return (prob_t)elem;
        }
      }
    }

  } else {
    // We have an uncompressed leaf; easy!
    // All uncompressed leafs start at zero for now
    if ((zq >= 0) && (zq < map->nz)) {
      return leaf->u.prob[zq];
    } else {
      return 0;
    }
  }
}

void colmap_iterate_occupied_cells(colmap_t *map, void (*cb)(int32_t x, int32_t y, int32_t z))
{
  for (int32_t i=0; i<N; i++) {
    for (int32_t j=0; j<N; j++) {
      if (map->root->child[i][j]) {
        // Pre-sign-extend for negative x and y values
        int32_t x = ((i & (N>>1)) ? ((0xFFFFFFFF << LOG_N) | i) : i);
        int32_t y = ((j & (N>>1)) ? ((0xFFFFFFFF << LOG_N) | j) : j);
        iterate_occupied_cells(map->root->child[i][j], map->depth-1, x, y, cb);
      }
    }
  }
}

/*
// print/debug
// -----------------------------------------------------------------------------------
void print_leaf(leaf_t *leaf)
{
  if (leaf->hdr & HDR_COMPRESSED) {
    printf("Compressed leaf:\n\tStart at: %04X\n", leaf->hdr >> HDR_START_SHIFT);

    uint32_t i = 0;
    int16_t elem;
    compressed_leaf_t *comp = (compressed_leaf_t*)leaf;
    while (1) {
      elem = comp->elem[i++];
      if (elem & ELEM_RUN) {
        switch (elem & ELEM_TYPE_MASK) {
          case ELEM_UNKNOWN:
            printf("\tRun len %u: UNKNOWN %s\n", elem >> ELEM_RUNLEN_SHIFT, (elem & ELEM_TERM) ? "TERM" : "");
            break;
          case ELEM_PROB_MIN:
            printf("\tRun len %u: MIN %s\n", elem >> ELEM_RUNLEN_SHIFT, (elem & ELEM_TERM) ? "TERM" : "");
            break;
          case ELEM_PROB_MAX:
            printf("\tRun len %u: MAX %s\n", elem >> ELEM_RUNLEN_SHIFT, (elem & ELEM_TERM) ? "TERM" : "");
            break;
          case ELEM_NORMAL:
            printf("\tRun len %u: %hd %s\n", elem >> ELEM_RUNLEN_SHIFT, (int16_t)comp->elem[i], (elem & ELEM_TERM) ? "TERM" : "");
            i++;
            break;
        }

        if (elem & ELEM_TERM) { break; }

      } else {
        printf("\tSingle: %hd\n", (int16_t)elem);
      }
    }

  } else {
    printf("Uncompressed leaf:\n");
    for (uint32_t i=0; i<NZ; i++) {
      printf("\t%04X prob: %d\n", i, ((uncompressed_leaf_t*)leaf)->prob[i]);
    }
  }
}

void calc_leaf_stats(compressed_leaf_t *leaf, leaf_stats_t *ls)
{
  // Calc stats of a single leaf
  assert(leaf->hdr & HDR_COMPRESSED);

  ls->n_runs = 0;
  ls->size32 = sizeof(compressed_leaf_t);
  ls->size16 = sizeof(compressed_leaf_t);

  uint32_t i = 0;
  elem_t elem;

  do {
    elem = leaf->elem[i++];
    if (elem & ELEM_RUN) {
      if ((elem & ELEM_TYPE_MASK) == ELEM_NORMAL) {
        if ((leaf->elem[i] == (elem_t)PROB_MAX) || (leaf->elem[i] == (elem_t)PROB_MIN) || !leaf->elem[i]) {
          ls->size16 += 2;
          ls->size32 += 2;
        } else {
          ls->size16 += 4;
          ls->size32 += 6;
        }
        i++;
      } else {
        ls->size16 += 2;
        ls->size32 += 2;
      }
    } else {
      if ((leaf->elem[i-1] == (elem_t)PROB_MAX) || (leaf->elem[i-1] == (elem_t)PROB_MIN) || !leaf->elem[i-1]) {
        ls->size16 += 2;
        ls->size32 += 2;
      } else {
        ls->size16 += 2;
        ls->size32 += 4;
      }
    }
    ls->n_runs++;
  } while (!((elem & (ELEM_RUN | ELEM_TERM)) == (ELEM_RUN | ELEM_TERM)));

}

void calc_tree_stats(tree_node_t *node, uint32_t depth, stats_t *s)
{
  // Iterate through tree and calculate stats (incl leaf stats)

  if (!depth) {
    //print_leaf((leaf_t*)node);
    if (((leaf_t*)node)->hdr & HDR_COMPRESSED) {
      leaf_stats_t ls;
      calc_leaf_stats((compressed_leaf_t*)node, &ls);
      s->comp_leaf_n_runs += ls.n_runs;
      s->comp_leaf_size32 += ls.size32;
      s->comp_leaf_size16 += ls.size16;
      s->n_comp_leafs++;
    } else {
      s->n_uncomp_leafs++;
    }
  } else {
    s->n_inner_nodes++;
    for (int32_t i=0; i<N; i++) {
      for (int32_t j=0; j<N; j++) {
        if (node->child[i][j]) {
          calc_tree_stats(node->child[i][j], depth-1, s);
        }
      }
    }
  }
}

void print_stats(tree_node_t *root)
{
  stats_t s = {0, 0, 0, 0, 0, 0};

  calc_tree_stats(root, DEPTH, &s);
  printf("Mem usage: %d bytes\n", mem_usage);
  printf("Comp leafs: %d\n", s.n_comp_leafs);
  printf("Uncomp leafs: %d\n", s.n_uncomp_leafs);
  printf("Total leafs: %d\n", s.n_comp_leafs+stats.n_uncomp_leafs);
  printf("Inner nodes: %d\n", s.n_inner_nodes);
  printf("Total size of inner nodes (32-bit): %u\n", s.n_inner_nodes*sizeof(tree_node_t));
  printf("Total size of inner nodes (64-bit): %u\n", s.n_inner_nodes*sizeof(tree_node_t)*2);
  printf("Total size of leafs: %u\n", mem_usage - s.n_inner_nodes*sizeof(tree_node_t));
  printf("Total runs in compressed leafs: %d\n", s.comp_leaf_n_runs);
  printf("Total size of compressed leafs (16-bit): %d\n", s.comp_leaf_size16);
  printf("Total size of compressed leafs (32-bit): %d\n", s.comp_leaf_size32);
  printf("Total size (16-bit probs, 64-bit ptrs): %d\n", s.comp_leaf_size16 + s.n_uncomp_leafs*(2+NZ*2) + s.n_inner_nodes*sizeof(tree_node_t)*2);
  printf("Total size (32-bit probs, 32-bit ptrs): %d\n", s.comp_leaf_size32 + s.n_uncomp_leafs*(2+NZ*4) + s.n_inner_nodes*sizeof(tree_node_t));
  printf("Total size (32-bit probs, 64-bit ptrs): %d\n\n", s.comp_leaf_size32 + s.n_uncomp_leafs*(2+NZ*4) + s.n_inner_nodes*sizeof(tree_node_t)*2);
}

void write_uncompressed_tree_to_file(tree_node_t *node, uint32_t depth, FILE *f)
{
  static int32_t ix = 0, iy = 0;

  if (!depth) {
    uncompressed_leaf_t *leaf = (uncompressed_leaf_t*)node;
    // Only works with uncompressed leafs!!
    assert(!(leaf->hdr & HDR_COMPRESSED));
    for (int32_t z=0;z<NZ;z++) {
      if (leaf->prob[z]) {
        int32_t prob = leaf->prob[z];
        fwrite(&ix, sizeof(int32_t), 1, f);
        fwrite(&iy, sizeof(int32_t), 1, f);
        fwrite(&z, sizeof(int32_t), 1, f);
        fwrite(&prob, sizeof(int32_t), 1, f);
      }
    }
  } else {
    for (int32_t i=0; i<N; i++) {
      for (int32_t j=0; j<N; j++) {
        if (node->child[i][j]) {
          uint32_t shift = (depth-1)*LOG_N;
          ix = (ix & ~((N-1) << shift)) | (i << shift);
          iy = (iy & ~((N-1) << shift)) | (j << shift);
          write_uncompressed_tree_to_file(node->child[i][j], depth-1, f);
        }
      }
    }
  }
}

void write_compressed_tree_to_file(tree_node_t *node, uint32_t depth, FILE *f)
{
  static int32_t ix = 0, iy = 0;

  if (!depth) {
    // Only works with compressed leafs!!
    compressed_leaf_t *cl = (compressed_leaf_t*)node;
    assert(cl->hdr & HDR_COMPRESSED);

    int32_t z = cl->hdr >> HDR_START_SHIFT;
    int32_t i = 0;
    int32_t prob, len;
    elem_t elem;

    while (1) {
      assert(i < MAX_ELEMS);
      elem = cl->elem[i++];

      if (elem & ELEM_RUN) {
        // It's a run
        len = elem >> ELEM_RUNLEN_SHIFT;

        switch (elem & ELEM_TYPE_MASK) {
          case ELEM_UNKNOWN:
            prob = 0;
            break;
          case ELEM_PROB_MIN:
            prob = PROB_MIN;
            break;
          case ELEM_PROB_MAX:
            prob = PROB_MAX;
            break;
          case ELEM_NORMAL:
            prob = (prob_t)(cl->elem[i++] & ~ELEM_RUN);
            break;
        }

        fwrite(&ix, sizeof(int32_t), 1, f);
        fwrite(&iy, sizeof(int32_t), 1, f);
        fwrite(&z, sizeof(int32_t), 1, f);
        fwrite(&len, sizeof(int32_t), 1, f);
        fwrite(&prob, sizeof(int32_t), 1, f);

        if (elem & ELEM_TERM) { break; }
        z += len;

      } else {
        // It's a normal prob
        prob = (prob_t)elem;
        len = 1;

        fwrite(&ix, sizeof(int32_t), 1, f);
        fwrite(&iy, sizeof(int32_t), 1, f);
        fwrite(&z, sizeof(int32_t), 1, f);
        fwrite(&len, sizeof(int32_t), 1, f);
        fwrite(&prob, sizeof(int32_t), 1, f);

        z++;
      }
    }

  } else {
    for (int32_t i=0; i<N; i++) {
      for (int32_t j=0; j<N; j++) {
        if (node->child[i][j]) {
          uint32_t shift = (depth-1)*LOG_N;
          ix = (ix & ~((N-1) << shift)) | (i << shift);
          iy = (iy & ~((N-1) << shift)) | (j << shift);
          write_compressed_tree_to_file(node->child[i][j], depth-1, f);
        }
      }
    }
  }
}

tree_node_t * read_map_from_file(char *xyzp_file, bool max_lik)
{
  FILE *f;
  tree_node_t *root = create_tree();

  printf("Reading xyzp file %s\n", xyzp_file);

  // Open the file
  if (!(f = fopen(xyzp_file, "rb"))) {
    printf("Couldn't open file.\n");
    return NULL;
  }

  // Read in the data
  int32_t n_pts;
  fread(&n_pts, sizeof(int32_t), 1, f);
  int32_t (*pts)[3] = malloc(n_pts*3*sizeof(int32_t));
  int32_t *probs = malloc(n_pts*sizeof(int32_t));
  fread(pts, 3*sizeof(int32_t), n_pts, f);
  fread(probs, sizeof(int32_t), n_pts, f);

  printf("Inserting...\n");
  for (uint32_t i=0; i<n_pts; i++) {
    //Probs must always have 2 LSBs clear!!
    if (max_lik) {
      if (probs[i] > 0) { probs[i] = PROB_MAX; }
      else if (probs[i] < 0) { probs[i] = PROB_MIN; }
    }
    update_cell_direct(root, pts[i], probs[i]);
  }
  printf("Inserted %d cells\n", n_pts);

  fclose(f);
  free(pts);
  free(probs);

  return root;
}

void insert_scans_from_file(tree_node_t *root, char *scangraph_file)
{
  FILE *f;

  printf("Reading scan graph file %s\n", scangraph_file);

  // Open the file
  if (!(f = fopen(scangraph_file, "rb"))) {
    printf("Couldn't open file.\n");
    return;
  }

  uint32_t n_frames;
  fread(&n_frames, sizeof(uint32_t), 1, f);

  scan_t *scans = malloc(sizeof(scan_t)*MAX_SCANS_PER_FRAME);

  for (uint32_t i=0; i<n_frames; i++) {
    uint32_t n_scans;
    float pos[3];

    fread(&n_scans, sizeof(uint32_t), 1, f);
    fread(pos, sizeof(float), 3, f);
    pos[2] += 5.0; // get it above zero
    fread(scans, sizeof(scan_t), n_scans, f);

    for (uint32_t j=0; j<n_scans; j++) {
      //if (scans[j].conf > 32768.0) {
        // TODO: incorporate scaling in insert_ray
        scans[j].pos[2] += 5.0; // get it above zero
        FOR3 { scans[j].pos[_] = scans[j].pos[_]*10.0; }
        insert_ray(root, pos, scans[j].pos);
      //}
    }
    printf("Inserted %u scans (frame %u/%u).\n", n_scans, i+1, n_frames);
  }
  fclose(f);
}

void visualize_occupied_cells(tree_node_t *root, void (*callback)(float x, float y, float z))
{
  // Iterates over all leaves and produces a callback for each occupied cell
  // Works regardless of whether compressed or not
}
*/
    
