// ----------------------------------------------------------------------------------
//
//  Filename:     colmap.h
//  Author:       Alex Fisher <alex.fisher@rmit.edu.au>
//  Description:  Probablistic mapping
//
// ----------------------------------------------------------------------------------

#ifndef COLMAP_H
#define COLMAP_H

// Includes
// ----------------------------------------------------------------------------------
#include <stdint.h>

// Defines
// -----------------------------------------------------------------------------------
#define N 16
#define LOG_N 4

// Typedefs
// -----------------------------------------------------------------------------------
typedef int16_t prob_t;
typedef uint16_t elem_t;

typedef struct tree_node_t {
  struct tree_node_t *child[N][N];
  //uint8_t max_like[N][N];
} tree_node_t;

typedef struct {
  uint16_t hdr;
  prob_t prob[]; // NZ
} uncompressed_leaf_t;

typedef struct {
  uint16_t hdr;
  elem_t elem[]; // MAX_ELEMS (= NZ+8)
} compressed_leaf_t;

typedef union {
  uint16_t hdr;
  uncompressed_leaf_t u;
  compressed_leaf_t c;
} leaf_t;

typedef struct {
  uint32_t n_comp_leafs;
  uint32_t n_uncomp_leafs;
  uint32_t n_inner_nodes;
  uint32_t comp_leaf_n_runs;
  uint32_t comp_leaf_size32;
  uint32_t comp_leaf_size16;
} stats_t;

typedef struct {
  uint32_t n_runs;
  uint32_t size32;
  uint32_t size16;
} leaf_stats_t;

typedef struct {
  float pos[3];
  float amp;
  float conf;
} scan_t;

typedef struct {
  tree_node_t *root;
  float inv_cell_size;
  float min_z;
  float max_scan_dist_sq;
  uint32_t depth, nz;
} colmap_t;

// Prototypes
// -----------------------------------------------------------------------------------
#ifdef __cplusplus
extern "C" {
#endif

colmap_t * colmap_create(float cell_size, float min_z, uint32_t nz, float max_scan_dist);
void colmap_delete(colmap_t *map);
void colmap_compress(colmap_t *map);
void colmap_decompress(colmap_t *map);
void colmap_insert_ray(colmap_t *map, float origin[3], float point[3]);
void colmap_insert_pcl(colmap_t *map, float origin[3], float points[][3], uint32_t n_pts);
prob_t colmap_query_cell(colmap_t *map, float pt[3]);
void colmap_iterate_occupied_cells(colmap_t *map, void (*cb)(int32_t x, int32_t y, int32_t z));

#ifdef __cplusplus
}
#endif

#endif

