#include <ros/ros.h>
#include <visualization_msgs/MarkerArray.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/ColorRGBA.h>
#include <std_srvs/Empty.h>
#include <pcl/point_types.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/passthrough.h>
#include <pcl_conversions/pcl_conversions.h>
#include <tf/transform_listener.h>
#include <tf/message_filter.h>
#include <message_filters/subscriber.h>
#include "colmap.h"


class ColmapServer {
	public:
    // Public for callback access
    float _cell_size, _min_z, _max_scan_dist;
    int _nz;
    visualization_msgs::Marker _marker;

		ColmapServer();
	private:
    ros::NodeHandle _nh;
    ros::ServiceServer _reset_srv;
    ros::Publisher _marker_pub;
    message_filters::Subscriber<sensor_msgs::PointCloud2> *_pcl_sub;
    tf::MessageFilter<sensor_msgs::PointCloud2> *_tf_pcl_sub;
    tf::TransformListener _tf_listener;
    std::string _map_frame;
    colmap_t *_colmap;

		void pclCallback(const sensor_msgs::PointCloud2::ConstPtr& cloud);
    bool resetCallback(std_srvs::Empty::Request& req, std_srvs::Empty::Response& resp);
};
		
// Pointer to access class from callback
// TODO: Fix this, very hacky
static ColmapServer *cs;

ColmapServer::ColmapServer() : _nh("~"), _cell_size(0.1)
{
  // Initialize params
  _nh.param<std::string>("map_frame", _map_frame, "map");
  _nh.param("cell_size", _cell_size, 0.1f);
  _nh.param("min_z", _min_z, 0.0f);
  _nh.param("nz", _nz, 96);
  _nh.param("max_scan_dist", _max_scan_dist, 30.0f);

  // Initialize publishers and subscribers
  _marker_pub = _nh.advertise<visualization_msgs::Marker>("occupied_cells_vis", 1, true);
  _pcl_sub = new message_filters::Subscriber<sensor_msgs::PointCloud2> (_nh, "/cloud_in", 5);
  _tf_pcl_sub = new tf::MessageFilter<sensor_msgs::PointCloud2> (*_pcl_sub, _tf_listener, _map_frame, 5);
  _tf_pcl_sub->registerCallback(boost::bind(&ColmapServer::pclCallback, this, _1));

  // Advertise the reset service
  _reset_srv = _nh.advertiseService("reset", &ColmapServer::resetCallback, this);

  // Initialize visualization marker message
  _marker.header.frame_id = _map_frame;
  _marker.ns = "colmap";
  _marker.id = 0;
  _marker.type = visualization_msgs::Marker::CUBE_LIST;
  _marker.action = visualization_msgs::Marker::ADD;
  _marker.scale.x = _cell_size;
  _marker.scale.y = _cell_size;
  _marker.scale.z = _cell_size;
  _marker.color.r = 1.0;
  _marker.color.g = 1.0;
  _marker.color.b = 1.0;
  _marker.color.a = 1.0;

  // Create the colmap
  _colmap = colmap_create(_cell_size, _min_z, _nz, _max_scan_dist);

  // Init global pointer
  cs = this;

}

extern "C" void visCallback(int32_t x, int32_t y, int32_t z)
{
  geometry_msgs::Point p;
  p.x = (float)x * cs->_cell_size;
  p.y = (float)y * cs->_cell_size;
  p.z = ((float)z * cs->_cell_size) + cs->_min_z;
  cs->_marker.points.push_back(p);
}

void ColmapServer::pclCallback(const sensor_msgs::PointCloud2::ConstPtr& cloud)
{
  pcl::PointCloud<pcl::PointXYZ> pc;
  pcl::fromROSMsg(*cloud, pc);

  // Get the transform from sensor -> world
  tf::StampedTransform sensorToWorld;
  try {
    _tf_listener.lookupTransform(_map_frame, cloud->header.frame_id, cloud->header.stamp, sensorToWorld);
  } catch(tf::TransformException& ex){
    ROS_ERROR_STREAM( "Transform error of sensor data " << ex.what());
    return;
  }

  tfScalar matr[4][4];
  sensorToWorld.getOpenGLMatrix((tfScalar*)matr);

  // Allocate mem for the transformed points
  float (*pts)[3] = (float (*)[3])(new float[pc.size() * 3]);

  // Apply the transform
  int i = 0;
  for (pcl::PointCloud<pcl::PointXYZ>::const_iterator it = pc.begin(); it != pc.end(); ++it) {
    pts[i][0] = matr[0][0]*it->x + matr[1][0]*it->y + matr[2][0]*it->z + matr[3][0];
    pts[i][1] = matr[0][1]*it->x + matr[1][1]*it->y + matr[2][1]*it->z + matr[3][1];
    pts[i][2] = matr[0][2]*it->x + matr[1][2]*it->y + matr[2][2]*it->z + matr[3][2];
    i++;
  }

  // Sensor origin in global frame location
  float origin[3];
  origin[0] = matr[3][0]; origin[1] = matr[3][1]; origin[2] = matr[3][2];

  // Insert the scans
  ros::WallTime start_time = ros::WallTime::now();
  colmap_insert_pcl(_colmap, origin, pts, pc.size());
  ros::WallTime start_time2 = ros::WallTime::now();
  colmap_compress(_colmap);
  double insert_time = (start_time2 - start_time).toSec();
  double compress_time = (ros::WallTime::now() - start_time2).toSec();
  ROS_DEBUG("Ins/comp %ld scans in %f sec %f sec", pc.size(), insert_time, compress_time);

  // Clean up
  delete [] pts;

  // Update the visualization
  start_time = ros::WallTime::now();
  colmap_iterate_occupied_cells(_colmap, visCallback);
  double iterate_time = (ros::WallTime::now() - start_time).toSec();
  ROS_DEBUG("Iterate %f sec", iterate_time);
  _marker.header.stamp = ros::Time::now();
  _marker_pub.publish(_marker);
  _marker.points.clear();

}

bool ColmapServer::resetCallback(std_srvs::Empty::Request& req, std_srvs::Empty::Response& resp)
{
  // Delete the old map and start anew
  colmap_delete(_colmap);
  _colmap = colmap_create(_cell_size, _min_z, _nz, _max_scan_dist);
  return true;
}

int main(int argc, char *argv[])
{
  ros::init(argc, argv, "colmap_server");
  ColmapServer server;

  ros::spin();

  return 0;
}

